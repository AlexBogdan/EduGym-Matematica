package Elements;

public class MathElement extends Element {

	public Element left = null;
	public Element mid = null;
	public Element right = null;
	
	public MathElement() {
		
	}
	
	public MathElement(final Element mid) {
		this.mid = mid;
	}
	
	public MathElement(final Element left, final Element right) {
		this.left = left;
		this.right = right;
	}
	
	public MathElement(final Element left, final Element mid, final Element right) {
		this.left = left;
		this.mid = mid;
		this.right = right;
	}
}
