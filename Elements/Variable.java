package Elements;

import Numere.Natural;
import Numere.Numar;
import Operatori.Equal;
import Operatori.NonEqual;
import Operatori.Positive;

public class Variable extends MathElement {

		public String name = null;
		public MathElement value = null;
		public Numar coef = new Natural(1);
		public Numar pow = new Natural(1);
		
		public Variable(final String name) {
			this.name = name;
		}
		
		public Variable(final String name, final Numar coef) {
			this.name = name;
			this.coef = coef;
		}
		
		public Variable(final String name, final Numar coef, final Numar pow) {
			this.name = name;
			this.coef = coef;
			this.pow = pow;
		}
		
		public Variable(final String name, final Numar coef, final Numar pow, final MathElement value) {
			this.name = name;
			this.value = value;
			this.coef = coef;
			this.pow = pow;
		}
		
		public String toString() {
			String result = "";
			
//			if (Positive.calculate(coef) && NonEqual.calculate(coef, new Natural(1))) {
//				result += "+";
//			}
			
			if (NonEqual.calculate(coef, new Natural(1))) {
				result += coef;
			}
			
			if (NonEqual.calculate(pow, new Natural(1))) {
				if (NonEqual.calculate(pow,  new Natural(0))) {
					result += name  + "^{" + pow + "}";
				}
			} else {
				result += name;
			}
			
			if (Equal.calculate(pow, new Natural(0)) && Equal.calculate(coef, new Natural(1))) {
				result += new Natural(1);
			}
			
			return result;
		}
}
