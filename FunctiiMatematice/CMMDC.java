package FunctiiMatematice;

import Numere.*;
import Operatori.*;

public class CMMDC {
	
	private static int gcd(int a, int b) {
		while (b != 0) {
	        int temp = b;
	        b = a % b;
	        a = temp;
	    }
		return a;
	}
	
	public static Natural calculate(Numar n1, Numar n2) {
		if (! (n1 instanceof Natural) || (n1 instanceof Intreg)) {
			return null;
		}
		if (! (n2 instanceof Natural) || (n2 instanceof Intreg)) {
			return null;
		}
		
		int a = (int) n1.value;
		int b = (int) n2.value;
		
	    return new Natural((double) Math.abs(gcd(a, b)));
	}
}
