import Algebra.Ecuation1;
import Algebra.MathExpression;
import Algebra.Polinom;
import Checker.*;
import Elements.Variable;
import FunctiiMatematice.CMMDC;
import Geometrie.Angle;
import Numere.*;
import Operatori.*;

/**
 * Proiectul EduGym - Matematica ---> 1976 linii de cod
 * @author Alex-Bogdan Andrei
 *
 */
public final class Main {

	public static void main(String[] args) {
		System.out.println("EduGym --- versiunea 0.00");
		System.out.println();
		
		GeneralCheck.start(true);
		
		Polinom p = new Polinom();
		p.setVariable(new Variable("x", new Intreg(2), new Natural(2)));
		p.setVariable(new Variable("x", new Natural(-3), new Natural(1)));
		p.setVariable(new Variable("x", new Natural(1), new Natural(0)));
//		p.setVariable(new Variable("x", new Intreg(3), new Natural(3)));
		
		Ecuation1 eq = new Ecuation1(new Variable("x", new Natural(4)), new Natural(6), new Natural(10));
		
		System.out.println(eq);
		System.out.println(eq.solve());
	}

}
