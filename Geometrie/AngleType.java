package Geometrie;

public enum AngleType {
	ZERO,   ACUTE,   RIGHT,    OBTUSE,    STRAIGHT,    REFLEX,    FULL
}
