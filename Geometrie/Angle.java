package Geometrie;

import Numere.*;
import Operatori.*;

public class Angle {
	
	private final Natural MAX_MS = new Natural(60);
	private final Natural MIN_MS = new Natural(0);
	
	public String nume = "Angle";
	public Intreg grade = null;
	public Natural minute = null;
	public Natural secunde = null;
	
	public Angle(Numar grade, Numar minute, Numar secunde) {
		this.grade = new Intreg(IntegralPart.calculate(grade));
		this.minute = new Natural(IntegralPart.calculate(minute));
		this.secunde = new Natural(IntegralPart.calculate(secunde));
		
		this.minute = new Natural(Plus.calculate(this.minute, Inmultire.calculate(MAX_MS, FractionalPart.calculate(grade))));
		this.secunde = new Natural(Plus.calculate(this.secunde, Inmultire.calculate(MAX_MS, FractionalPart.calculate(minute))));
		
		solveAngle();
	}
	
	public void destroy() {
		this.grade = null;
		this.minute = null;
		this.secunde = null;
	}
	
	public boolean isDestroyed() {
		return (grade == null && minute == null && secunde == null);
	}
	
	public boolean checkMinute() {
		return BetweenRightStrict.calculate(minute, MIN_MS, MAX_MS);	
	}
	
	public boolean checkSecunde() {
		return BetweenRightStrict.calculate(secunde, MIN_MS, MAX_MS);	
	}
	
	public void solveMinute() {
		if (GreaterEqual.calculate(minute, MAX_MS)) {
			grade = new Intreg(Plus.calculate(grade, IntegralPart.calculate(Impartire.calculate(minute, MAX_MS))));
			minute = new Natural(Rest.calculate(minute, MAX_MS));
			return;
		}
		
		if (Less.calculate(minute, MIN_MS)) {
			grade = new Intreg(Plus.calculate(grade, Minus.calculate(IntegralPart.calculate(Impartire.calculate(minute, MAX_MS)), new Natural(1))));
			minute = new Natural(Plus.calculate(MAX_MS, Rest.calculate(minute, MAX_MS)));
		}
	}
	
	public void solveSecunde() {
		if (GreaterEqual.calculate(secunde, MAX_MS)) {
			minute = new Natural(Plus.calculate(minute, IntegralPart.calculate(Impartire.calculate(secunde, MAX_MS))));
			secunde = new Natural(Rest.calculate(secunde, MAX_MS));
			return;
		}
		
		if (Less.calculate(secunde, MIN_MS)) {
			minute = new Natural(Plus.calculate(minute, Minus.calculate(IntegralPart.calculate(Impartire.calculate(secunde, MAX_MS)), new Natural(1))));
			secunde = new Natural(Plus.calculate(MAX_MS, Rest.calculate(secunde, MAX_MS)));
		}
	}
	
	public void solveAngle() {
		solveSecunde();
		solveMinute();
		
		solveSecunde();
		solveMinute();
	}
	
	public AngleType getType() {
		if (Equal.calculate(grade, new Natural(0)) &&
				 Equal.calculate(minute, new Natural(0)) &&
				 Equal.calculate(secunde, new Natural(0))) {
			return AngleType.ZERO;
		}
		if (Equal.calculate(grade, new Natural(90)) &&
				 Equal.calculate(minute, new Natural(0)) &&
				 Equal.calculate(secunde, new Natural(0))) {
			return AngleType.RIGHT;
		}
		if (Equal.calculate(grade, new Natural(180)) &&
				 Equal.calculate(minute, new Natural(0)) &&
				 Equal.calculate(secunde, new Natural(0))) {
			return AngleType.STRAIGHT;
		}
		if (Equal.calculate(grade, new Natural(360)) &&
				 Equal.calculate(minute, new Natural(0)) &&
				 Equal.calculate(secunde, new Natural(0))) {
			return AngleType.FULL;
		}
		if (BetweenStrict.calculate(grade, new Natural(0), new Natural(90))) {
			return AngleType.ACUTE;
		}
		if (BetweenStrict.calculate(grade, new Natural(90), new Natural(180))) {
			return AngleType.OBTUSE;
		}
		if (BetweenStrict.calculate(grade, new Natural(180), new Natural(360))) {
			return AngleType.REFLEX;
		}
		return null;
	}
	
	public static Angle getZeroAngle() {
		return new Angle(new Natural(0), new Natural(0), new Natural(0));
	}
	
	public static Angle getRightAngle() {
		return new Angle(new Natural(90), new Natural(0), new Natural(0)); 
	}
	
	public static Angle getStraightAngle() {
		return new Angle(new Natural(180), new Natural(0), new Natural(0)); 
	}
	
	public static Angle getFullAngle() {
		return new Angle(new Natural(360), new Natural(0), new Natural(0)); 
	}

	public Angle getComplement() {
		AngleType type = getType();
		if (type ==  AngleType.ZERO || type == AngleType.ACUTE || type == AngleType.RIGHT) {
			return Minus.calculate(getRightAngle(), this);
		} else {
			return null;
		}
	}
	
	public Angle getSuplement() {
		AngleType type = getType();
		if (type ==  AngleType.ZERO || type == AngleType.ACUTE || type == AngleType.RIGHT ||
				type == AngleType.OBTUSE || type == AngleType.STRAIGHT) {
			return Minus.calculate(getStraightAngle(), this);
		} else {
			return null;
		}
	}
	
	public String toString() {
		return nume + "   " + "[ " + grade + "  ,  " + minute + "  ,  " + secunde + "  ]";  
	}
}
