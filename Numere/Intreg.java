package Numere;

public class Intreg extends Numar {
	
	public Intreg(double value) {
		super(value);
		if (Math.abs(value - (int) value) > 0.000000001) {
			destroy();
		}
	}
	
	public Intreg(Numar n) {
		super(n.value);
		if (! n.isIntreg()) {
			destroy();
		}
	}

	public String toString() {
		return "" + (int) value;
	}
}
