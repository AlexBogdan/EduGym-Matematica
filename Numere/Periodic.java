package Numere;

public class Periodic extends Numar {
	
	public Natural perioada = null;
	
	/**
	 * Transforma numarul din forma cu perioada in fractie
	 */
	public void setFractie() {
		
	}
	
	public Periodic(double value, double perioada) {
		super(value);
		this.perioada = new Natural(perioada);
	}
	
	public Periodic(double perioada) {
		super(0);
		this.perioada = new Natural(perioada);
	}
	
	public String toString() {
		String result = "" + value;
		result += "(" + perioada + ")";
		
		return result;
	}
}
