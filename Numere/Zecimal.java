package Numere;

import Operatori.FractionalPart;
import Operatori.IntegralPart;

public class Zecimal extends Numar {

	public Zecimal(double value) {
		super(value);
	}

	public String toString() {
		return new Double(value).toString().replace('.', ',');
	}
}
