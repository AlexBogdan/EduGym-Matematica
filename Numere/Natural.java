package Numere;

public class Natural extends Numar{

	public Natural(double value) {
		super(value);
		if (Math.abs(value - (int) value) > 0.000000001 || value < 0) {
			destroy();
		}
	}
	
	public Natural(Numar n) {
		super(n.value);
		if (! n.isNatural()) {
			destroy();
		}
	}
	
	public String toString() {
		return "" + (int) value;
	}
}