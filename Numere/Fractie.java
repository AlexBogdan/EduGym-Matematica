package Numere;

public class Fractie extends Numar {

	public Fractie(double numarator) {
		super(new Numar(numarator));
	}
	
	public Fractie(double numarator, double numitor) {
		super(new Numar(numarator), new Numar(numitor));
	}
	
	public String toString() {
		return "\\dfrac {" + numarator + "}{" + numitor + "}";
	}
}
