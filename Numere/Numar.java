package Numere;

import Elements.MathElement;

public class Numar extends MathElement {
	
    public double value;
	
	public Numar numarator = null;
	public Numar numitor = null;
	public Numar pow = null;
	public Numar coef = null;
	
	Numar() {
		
	}
	
	Numar(double value) {
		this.value = value;
		zecimalToFractie();
	}
	
	Numar(Numar numarator) {
		this.numarator = numarator;
		this.numitor = new Numar(1);
		fractieToZecimal();
	}
	
	Numar(Numar numarator, Numar numitor) {
		this.numarator = numarator;
		this.numitor = numitor;
		fractieToZecimal();
	}
	
	void setPower(double pow) {
		this.pow = new Numar(pow);
	}
	
	void destroy() {
		numarator = null;
		numitor = null;
	}
	
	public boolean isDestroyed() {
		if (numarator == null && numitor == null) {
			return true;
		}
		return false;
	}
	
	protected void zecimalToFractie() {
		if (numarator == null) {
			numarator = new Numar();
		}
		numarator.value = value;
		
		if (numitor == null) {
			numitor = new Numar();
		}
		numitor.value = 1;
		
		while (Math.abs((int) numarator.value - numarator.value) > 0.1) {
			numarator.value *= 10;
			numitor.value *= 10;
		}
		
		numarator.value = (int) numarator.value;
	}
	
	public void fractieToZecimal() {
		if (numarator != null && numitor != null) {
			value = numarator.value / numitor.value;
		}
	}
	
	public void simplificare() {
		
	}
	
	public void amplificare(Numar n) {
		
	}
	
	public boolean ireductibil() {
		
		return false;
	}
	
	public boolean isNatural() {
		return (Math.abs(value - (int) value) < 0.1 && value >= 0);
	}
	
	public boolean isIntreg() {
		return (Math.abs(value - (int) value) < 0.1);
	}
	
	public String toString() {
		return "" + value;
	}
}
