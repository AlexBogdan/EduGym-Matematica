package Numere;

public abstract class NumberFactory {
	
	public static Natural getNatural(double value) {
		if (Math.abs(value - (int) value) > 0.000000001 || value < 0) {
			return null;
		}
		else {
			return new Natural(value);
		}
	}
	
	public static Intreg getIntreg(double value) {
		if (Math.abs(value - (int) value) > 0.000000001) {
			return null;
		}
		else {
			return new Intreg(value);
		}
	}

	public static Zecimal getZecimal(double value) {
		return new Zecimal(value);
	}
	
	public static Fractie getFractie(double numarator) {
		return new Fractie(numarator);
	}
	public static Fractie getFractie(double numarator, double numitor) {
		return new Fractie(numarator, numitor);
	}
	
	public static Periodic getPeriodic(double perioada) {
		return new Periodic(perioada);
	}
	public static Periodic getPeriodic(double value, double perioada) {
		return new Periodic(value, perioada);
	}
}
