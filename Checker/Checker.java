package Checker;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import Numere.*;

/**
 * Verifica daca numerele N, Z, Q, R au fost implementate bine
 * @author Andrei Bogdan Alexandru
 *
 */
public class Checker {
	
	static Integer totalTests = 0;
	static Integer passedTests = 0;
	static boolean onlyFails = false;
	
	static void printImportantLine(final String text) {
		System.out.print(new String(new char[25]).replace("\0", "*"));
		System.out.print(new String(new char[30 - text.length()/2]).replace("\0", " "));;
		System.out.print(text);
		System.out.print(new String(new char[31 - text.length()/2]).replace("\0", " "));;
		System.out.println(new String(new char[25]).replace("\0", "*"));
	}
	
	static void printTitle(final String title) {
		if (onlyFails == false) {
			printImportantLine(title);
		}
		System.out.println();
	}
	
	static void printAll() {
		if (onlyFails == false) {
			System.out.println();
			printImportantLine("TOTAL :  " + passedTests + " / " + totalTests + "  teste trecute");
		}
	}
	
	static void printSection(final AtomicInteger passed, final AtomicInteger total) {
		if (onlyFails == false) {
			printImportantLine(passed + " / " + total + "  teste trecute");
			System.out.println();
		}
	}
	
	static void pass(String test, AtomicInteger passed, AtomicInteger total) {
		if (onlyFails == false) {
			test +="  " +  new String(new char[100 - test.length()]).replace("\0", "-") + "  [PASSED]" ;
			System.out.println(test);
		}

		total.set(total.get() + 1);
		passed.set(passed.get() + 1);
		
		totalTests++;
		passedTests++;
	}
	
	static void fail(String test, AtomicInteger total) {
		test +="  " +  new String(new char[100 - test.length()]).replace("\0", "-") + "  [FAILED]" ;
		System.out.println(test);
		
		total.set(total.get() + 1);
		totalTests++;
	}
}
