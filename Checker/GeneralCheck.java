package Checker;

public abstract class GeneralCheck {
	
	/**
	 * 
	 * @param onlyFails = (true) Printeaza doar testele in care am avut fail | (false) printeaza toate testele
	 */
	public static void start(final boolean onlyFails) {
		CheckerNumere.start("Verificare Numere (Declarari / Restrictii)", onlyFails);
		CheckerNumere.start("Verificare Parte Intreaga", onlyFails);
		CheckerAngle.start("Verificare Unghiuri (Definire si Operatii)", onlyFails);
		
		Checker.printAll();
	}

}
