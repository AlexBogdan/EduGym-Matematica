package Checker;

import Numere.*;
import Operatori.Plus;

public final class CheckerPlus extends Checker {
	
	private static final Natural n = new Natural(3);
	private static final Intreg z = new Intreg(-2);
	private static final Zecimal q1 = new Zecimal(2.5);
	private static final Zecimal q2 = new Zecimal(-3.5);
	private static final Fractie f1 = new Fractie(4, 3);
	private static final Fractie f2 = new Fractie (-5, 6);
	
	private static void natural() {
		
		Numar rezultat;
		
		rezultat = Plus.calculate(n, z);
		if (rezultat.value == 1) {
			
		}
	}
	
	public static void start(final String title, boolean fails) {
		onlyFails = fails;
		
		printTitle("Verificare PLUS (Operator)");
		
	}
}
