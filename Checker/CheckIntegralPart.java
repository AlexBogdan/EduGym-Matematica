package Checker;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import Numere.*;
import Operatori.IntegralPart;

public final class CheckIntegralPart extends Checker{
	
	private static void testIntegralPart() {
		ArrayList<Numar> numere = new ArrayList<Numar>();
		numere.add(new Natural(1));
		numere.add(new Intreg(-2));
		numere.add(new Zecimal(2.3));
		numere.add(new Fractie(-3, 5));
		
		String test = "";
		AtomicInteger passed = new AtomicInteger(0);;
		AtomicInteger total = new AtomicInteger(0);
		int index = 0;
		
		for ( ; index < numere.size(); index++) {
			test = "[ParteIntreaga " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
			if ((int) numere.get(index).value == IntegralPart.calculate(numere.get(index)).value) {
				pass(test, total, passed);
			} else {
				fail(test, total);
			}
		}
		
		printSection(passed, total);
	}
	
	public static void start(final String title, boolean fails) {
		onlyFails = fails;
		
		printTitle(title);
		testIntegralPart();
	}
}
