package Checker;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import Numere.*;
import Operatori.*;
import Geometrie.Angle;

public final class CheckerAngle extends Checker {
	
	private static void testAngles() {
		Angle u1 = new Angle (new Intreg(2), new Natural(30), new Natural(14));
		Angle u2 = new Angle (new Intreg(47), new Natural(20), new Natural(54));
		Angle u3 = new Angle (new Intreg(100), new Natural(59), new Natural(59));
		
		String test = "";
		AtomicInteger passed = new AtomicInteger(0);;
		AtomicInteger total = new AtomicInteger(0);
		
		test = "[Angle 1] " + u1;
		if (u1.grade != null) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		test = "[Angle 2] " + u2;
		if (u2.grade != null) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		test = "[Angle 3] " + u3;
		if (u3.grade != null) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle plus1 = Plus.calculate(u1, u2);
		test = "[Plus U1 U2] " + plus1;
		if (Equal.calculate(plus1.grade, new Natural(49)) &&
			 Equal.calculate(plus1.minute, new Natural(51)) &&
			 Equal.calculate(plus1.secunde, new Natural(8))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle plus2 = Plus.calculate(u1, u3);
		test = "[Plus U1 U3] " + plus2;
		if (Equal.calculate(plus2.grade, new Natural(103)) &&
			 Equal.calculate(plus2.minute, new Natural(30)) &&
			 Equal.calculate(plus2.secunde, new Natural(13))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle plus3 = Plus.calculate(u2, u3);
		test = "[Plus U2 U3] " + plus3;
		if (Equal.calculate(plus3.grade, new Natural(148)) &&
			 Equal.calculate(plus3.minute, new Natural(20)) &&
			 Equal.calculate(plus3.secunde, new Natural(53))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle minus1 = Minus.calculate(u2, u1);
		test = "[Minus U2 U1] " + minus1;
		if (Equal.calculate(minus1.grade, new Natural(44)) &&
			 Equal.calculate(minus1.minute, new Natural(50)) &&
			 Equal.calculate(minus1.secunde, new Natural(40))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle minus2 = Minus.calculate(u3, u2);
		test = "[Minus U3 U2] " + minus2;
		if (Equal.calculate(minus2.grade, new Natural(53)) &&
			 Equal.calculate(minus2.minute, new Natural(39)) &&
			 Equal.calculate(minus2.secunde, new Natural(5))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle minus3 = Minus.calculate(u2, u3);
		test = "[Minus U2 U3] " + minus3;
		if (Equal.calculate(minus3.grade, new Intreg(-54)) &&
			 Equal.calculate(minus3.minute, new Natural(20)) &&
			 Equal.calculate(minus3.secunde, new Natural(55))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle inmultire1 = Inmultire.calculate(u3, new Natural(2));
		test = "[Inmultire U3 2] " + inmultire1;
		if (Equal.calculate(inmultire1.grade, new Intreg(201)) &&
			 Equal.calculate(inmultire1.minute, new Natural(59)) &&
			 Equal.calculate(inmultire1.secunde, new Natural(58))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle inmultire2 = Inmultire.calculate(u1, new Fractie(3, 2));
		test = "[Inmultire U1 3/2] " + inmultire2;
		if (Equal.calculate(inmultire2.grade, new Intreg(3)) &&
			 Equal.calculate(inmultire2.minute, new Natural(45)) &&
			 Equal.calculate(inmultire2.secunde, new Natural(21))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle impartire1 = Impartire.calculate(u1, new Natural(2));
		test = "[Impartire U1 2] " + impartire1;
		if (Equal.calculate(impartire1.grade, new Intreg(1)) &&
			 Equal.calculate(impartire1.minute, new Natural(15)) &&
			 Equal.calculate(impartire1.secunde, new Natural(7))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle impartire2 = Impartire.calculate(u2, new Natural(2));
		test = "[Impartire U2 2] " + impartire2;
		if (Equal.calculate(impartire2.grade, new Intreg(23)) &&
			 Equal.calculate(impartire2.minute, new Natural(40)) &&
			 Equal.calculate(impartire2.secunde, new Natural(27))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle c1 = u1.getComplement();
		test = "[Complement U1] " + c1;
		if (Equal.calculate(c1.grade, new Intreg(87)) &&
			 Equal.calculate(c1.minute, new Natural(29)) &&
			 Equal.calculate(c1.secunde, new Natural(46))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle c3 = u3.getComplement();
		test = "[Complement U3] " + c3;
		if (c3 == null) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		Angle s3 = u3.getSuplement();
		test = "[Suplement U3] " + s3;
		if (Equal.calculate(s3.grade, new Intreg(79)) &&
			 Equal.calculate(s3.minute, new Natural(0)) &&
			 Equal.calculate(s3.secunde, new Natural(1))) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		test = "[Equal]";
		if (Equal.calculate(u1, u1)) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		test = "[NonEqual]";
		if (NonEqual.calculate(u1, u2)) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		test = "[Less]";
		if (Less.calculate(u1, u2)) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		test = "[Greater]";
		if (Greater.calculate(u3, u2)) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		test = "[Between]";
		if (BetweenStrict.calculate(u2, u1, u3)) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		
		printSection(passed, total);
	}
	
	public static void start(final String title, boolean fails) {
		onlyFails = fails;
		
		printTitle(title);
		
		testAngles();
	}
}
