package Checker;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import Numere.*;

public final class CheckerNumere extends Checker {
	
	/**
	 * Testele pe numere naturale
	 */
	private static void testNatural() {
		ArrayList<Natural> numere = new ArrayList<Natural>();
		numere.add(new Natural(0));
		numere.add(new Natural(2));
		numere.add(new Natural(-3));
		numere.add(new Natural(1.5));
		numere.add(new Natural(-4.4));
		
		String test = "";
		AtomicInteger passed = new AtomicInteger(0);;
		AtomicInteger total = new AtomicInteger(0);
		int index = 0;
		
		test = "[Natural " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == 0 && numere.get(index).numarator.value == 0 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		test = "[Natural " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == 2 && numere.get(index).numarator.value == 2 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		for ( ; index < numere.size(); index++) {
			test = "[Natural " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
			if (numere.get(index).isDestroyed()) {
				pass(test, total, passed);
			} else {
				fail(test, total);
			}
		}
		
		printSection(passed, total);
	}
	
	/**
	 * Testele pe numere intregi
	 */
	private static void testIntreg() {
		ArrayList<Intreg> numere = new ArrayList<Intreg>();
		numere.add(new Intreg(0));
		numere.add(new Intreg(2));
		numere.add(new Intreg(-3));
		numere.add(new Intreg(1.5));
		numere.add(new Intreg(-4.4));
		
		String test = "";
		AtomicInteger passed = new AtomicInteger(0);;
		AtomicInteger total = new AtomicInteger(0);
		int index = 0;
		
		test = "[Intreg " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == 0 && numere.get(index).numarator.value == 0 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		test = "[Intreg " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == 2 && numere.get(index).numarator.value == 2 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		test = "[Intreg " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == -3 && numere.get(index).numarator.value == -3 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		for ( ; index < numere.size(); index++) {
			test = "[Intreg " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
			if (numere.get(index).isDestroyed()) {
				pass(test, total, passed);
			} else {
				fail(test, total);
			}
		}
		printSection(passed, total);
	}
	
	/**
	 * Testele pe numere zecimale (virgula)
	 */
	private static void testZecimal() {
		ArrayList<Zecimal> numere = new ArrayList<Zecimal>();
		numere.add(new Zecimal(0));
		numere.add(new Zecimal(2));
		numere.add(new Zecimal(-3));
		numere.add(new Zecimal(1.5));
		numere.add(new Zecimal(-4.4));
		
		String test = "";
		AtomicInteger passed = new AtomicInteger(0);;
		AtomicInteger total = new AtomicInteger(0);
		int index = 0;
		
		test = "[Zecimal " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == 0 && numere.get(index).numarator.value == 0 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		test = "[Zecimal " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == 2 && numere.get(index).numarator.value == 2 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		test = "[Zecimal " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == -3 && numere.get(index).numarator.value == -3 && numere.get(index).numitor.value == 1) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		test = "[Zecimal " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == 1.5 && numere.get(index).numarator.value == 15 && numere.get(index).numitor.value == 10) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		test = "[Zecimal " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
		if (numere.get(index).value == -4.4 && numere.get(index).numarator.value == -44 && numere.get(index).numitor.value == 10) {
			pass(test, total, passed);
		} else {
			fail(test, total);
		}
		index ++;
		
		printSection(passed, total);
	}
	
	 /**
	  * Testele pe fractii
	 */
	private static void testFractie() {
		ArrayList<Fractie> numere = new ArrayList<Fractie>();
		numere.add(new Fractie(0));
		numere.add(new Fractie(2, 3));
		numere.add(new Fractie(-3, 5));
		numere.add(new Fractie(1.5 , 4));
		numere.add(new Fractie(-4.4, 1.1));
		
		String test = "";
		AtomicInteger passed = new AtomicInteger(0);;
		AtomicInteger total = new AtomicInteger(0);
		int index = 0;
		
		for ( ; index < numere.size(); index++) {
			test = "[Fractie " + index + "] " + numere.get(index).value + " | " + numere.get(index).numarator + " / " + numere.get(index).numitor;
			if (numere.get(index).value == numere.get(index).numarator.value / numere.get(index).numitor.value
					&& numere.get(index).numitor.value != 0) {
				pass(test, total, passed);
			} else {
				fail(test, total);
			}
		}
		
		printSection(passed, total);
	}
	
	public static void start(final String title, boolean fails) {
		onlyFails = fails;
		
		printTitle(title);
		
		testNatural();
		testIntreg();
		testZecimal();
		testFractie();
	}
}
