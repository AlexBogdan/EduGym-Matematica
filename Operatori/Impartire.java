package Operatori;

import Elements.Element;
import Elements.MathElement;
import Geometrie.Angle;
import Numere.*;

public final class Impartire extends MathElement {

	public Impartire() {
		
	}
	
	public Impartire(final Element left, final Element right) {
		super(left, right);
	}
	
	public static Numar calculate(Numar n1, Numar n2) {
		return Inmultire.calculate(n1, Invers.invers(n2));
	}
	
	public static Angle calculate(Angle u, final Numar n) {
		return new Angle(new Intreg(Impartire.calculate(u.grade, n)),
				new Natural(Impartire.calculate(u.minute, n)),
				new Natural(Impartire.calculate(u.secunde, n)));
	}

	public String toString() {
		if (left != null && right != null) {
			return left + " : " + right;
		} else {
			return ":";
		}
	}
}
