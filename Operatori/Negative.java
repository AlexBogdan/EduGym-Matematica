package Operatori;

import Elements.Element;
import Elements.MathElement;
import Numere.Natural;
import Numere.Numar;

public class Negative extends MathElement {

	public Negative(final MathElement e) {
		this.mid = e;
	}
	
	public boolean calculate(final Numar n) {
		return LessEqual.calculate(n, new Natural(0));
	}
	
	public String toString() {
		return (new LessEqual(mid, new Natural(0))).toString();
	}
}
