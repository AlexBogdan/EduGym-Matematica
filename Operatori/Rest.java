package Operatori;

import Elements.MathElement;
import Numere.Intreg;
import Numere.Numar;

public final class Rest extends MathElement {
	
	public Rest() {
		
	}
	
	public static Numar calculate(Numar n1, Numar n2) {
		return new Intreg(n1.value % n2.value);
	}
}
