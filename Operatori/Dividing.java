package Operatori;

import Elements.Element;
import Elements.MathElement;
import Numere.Natural;
import Numere.Numar;

public class Dividing extends MathElement{

		public Dividing() {
			
		}
		
		public Dividing(final Element left, final Element right) {
			super(left, right);
		}
		
		/**
		 * Verifica daca n1 divide n2
		 */
		public static boolean calculate(final Numar n1, final Numar n2) {
			return Equal.calculate(Rest.calculate(n2, n1), new Natural(0));
		}
		
		public String toString() {
			if (left != null && right != null) {
				return left + " \\mid " + right;
			} else {
				return "\\mid";
			}
		}
}
