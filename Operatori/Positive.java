package Operatori;

import Elements.Element;
import Elements.MathElement;
import Numere.Natural;
import Numere.Numar;

public class Positive extends MathElement {

	public Positive(final MathElement e) {
		this.mid = e;
	}
	
	public static boolean calculate(final Numar n) {
		return GreaterEqual.calculate(n, new Natural(0));
	}
	
	public String toString() {
		return (new GreaterEqual(mid, new Natural(0))).toString();
	}
}
