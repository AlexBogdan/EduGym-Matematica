package Operatori;

import Elements.Element;
import Elements.MathElement;
import Numere.Intreg;
import Numere.Numar;

public final class IntegralPart extends MathElement{
	
	public IntegralPart(final Element e) {
		super(e);
	}
	
	public static Numar calculate(Numar n) {
		return new Intreg((int) n.value);
	}
	
	public String toString() {
		if (mid != null) {
			return "[" + mid + "]";
		} else {
			return "";
		}
	}
}
