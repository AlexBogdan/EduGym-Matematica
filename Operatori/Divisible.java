package Operatori;

import Elements.Element;
import Elements.MathElement;
import Numere.Natural;
import Numere.Numar;

public final class Divisible extends MathElement{
	
	public Divisible() {
		
	}
	
	public Divisible(final Element left, final Element right) {
		super(left, right);
	}
	
	/**
	 * Verifica daca n1 este divizibil cu n2
	 */
	public static boolean calculate(final Numar n1, final Numar n2) {
		return Equal.calculate(Rest.calculate(n1, n2), new Natural(0));
	}
	
	public String toString() {
		if (left != null && right != null) {
			return left + " ⋮ " + right;
		} else {
			return "⋮";
		}
	}
}
