package Operatori;

import Elements.Element;
import Elements.MathElement;
import Geometrie.Angle;
import Numere.*;

public final class Plus extends MathElement{
	
	public Plus() {
		
	}
	
	public Plus(Element left, Element right) {
		super(left, right);
	}
	
	public static Numar calculate(Numar n1, Numar n2) {
		
		if (n1 instanceof Natural) {
			
			if (n2 instanceof Natural) {
				return new Natural(n1.value + n2.value);
			}
			if (n2 instanceof Intreg) {
				return new Intreg(n1.value + n2.value);
			}
			if (n2 instanceof Zecimal) {
				return new Zecimal(n1.value + n2.value);
			}
			if (n2 instanceof Fractie) {
				return new Fractie(n1.value * n2.numitor.value + n2.numarator.value,
												n2.numitor.value);
			}
//			if (n2 instanceof Periodic) {
//				return NumberFactory.getPeriodic(n1.value + n2.value,
//													 ((Periodic) n2).perioada.value);
//			}
		}
		if (n2 instanceof Natural) {
			return calculate(n2, n1);
		}
		
		// Adunare intre Intreg si orice
		if (n1 instanceof Intreg) {
			
			if (n2 instanceof Intreg) {
				return new Intreg(n1.value + n2.value);
			}
			if (n2 instanceof Zecimal) {
				return new Zecimal(n1.value + n2.value);
			}
			if (n2 instanceof Fractie) {
				return new Fractie(n1.value * n2.numitor.value + n2.numarator.value,
												n2.numitor.value);
			}
//			if (n2 instanceof Periodic) {
//				return new Periodic(n1.value + n2.value,
//													 ((Periodic) n2).perioada.value);
//			}
		}
		if (n2 instanceof Intreg) {
			return calculate(n2, n1);
		}
		
		// Adunare intre Zecimal si orice
		if (n1 instanceof Zecimal) {
			
			if (n2 instanceof Zecimal) {
				return new Zecimal(n1.value + n2.value);
			}
//			if (n2 instanceof Fractie) {
//				return NumberFactory.getFractie(n1.value * n2.numitor.value + n2.numarator.value,
//												n2.numitor.value);
//			}
//			if (n2 instanceof Periodic) {
//				return NumberFactory.getPeriodic(n1.value + n2.value,
//													 ((Periodic) n2).perioada.value);
//			}
		}
		if (n2 instanceof Zecimal) {
			return calculate(n2, n1);
		}
		
		// Adunare intre Fractie si orice
		if (n1 instanceof Fractie) {
			
			if (n2 instanceof Fractie) {
				return new Fractie(n1.value * n2.numitor.value + n2.numarator.value,
												n2.numitor.value);
			}
//			if (n2 instanceof Periodic) {
//				return NumberFactory.getPeriodic(n1.value + n2.value,
//													 ((Periodic) n2).perioada.value);
//			}
		}
		if (n2 instanceof Fractie) {
			return calculate(n2, n1);
		}
		
		return null;
	}
	
	public static Angle calculate(Angle u1, Angle u2) {
		return new Angle(Plus.calculate(u1.grade, u2.grade),
											 Plus.calculate(u1.minute, u2.minute),
											 Plus.calculate(u1.secunde, u2.secunde));
	}
	
	public String toString() {
		if (left != null && right != null) {
			return left + " + " + right;
		} else {
			return "+";
		}
	}
}
