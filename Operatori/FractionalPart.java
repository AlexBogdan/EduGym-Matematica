package Operatori;

import Elements.Element;
import Elements.MathElement;
import Numere.Numar;

public final class FractionalPart extends MathElement {
	
	public FractionalPart(final Element e) {
		super(e);
	}
	
	public static Numar calculate(Numar n) {
		return Minus.calculate(n, IntegralPart.calculate(n));
	}
	
	public String toString() {
		if (mid != null) {
			return "{" + mid + "}";
		} else {
			return "";
		}
	}
}
