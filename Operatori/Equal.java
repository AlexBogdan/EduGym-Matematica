package Operatori;

import Elements.Element;
import Elements.MathElement;
import Geometrie.Angle;
import Numere.Numar;

public final class Equal extends MathElement{
	
	public Equal() {
		
	}
	
	public Equal(final Element left, final Element right) {
		super(left, right);
	}
	
	public static boolean calculate(final Numar n1, final Numar n2) {
		return (Math.abs(n1.value - n2.value) < 0.000000001);
	}
	
	public static boolean calculate(final Angle u1, final Angle u2) {
		return Equal.calculate(u1.grade, u2.grade) &&
					 Equal.calculate(u1.minute, u2.minute) &&
					 Equal.calculate(u1.secunde, u2.secunde);
	}
	
	public String toString() {
		if (left != null && right != null) {
			return left + " = " + right;
		} else {
			return "=";
		}
	}
}
