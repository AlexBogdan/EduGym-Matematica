package Operatori;

import Elements.Element;
import Elements.MathElement;
import Geometrie.Angle;
import Numere.Numar;

public final class LessEqual extends MathElement {
	
	public LessEqual() {
		
	}
	
	public LessEqual(final Element left, final Element right) {
		super(left, right);
	}
	
	public static boolean calculate(final Numar n1, final Numar n2) {
		return Equal.calculate(n1, n2) || Less.calculate(n1, n2);
	}
	
	public static boolean calculate(final Angle u1, final Angle u2) {
		return Equal.calculate(u1, u2) || Less.calculate(u1, u2);
	}
	
	public String toString() {
		if (left != null && right != null) {
			return left + " \\le " + right;
		} else {
			return "\\le";
		}
	}
}
