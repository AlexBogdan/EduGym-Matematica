package Operatori;

import Numere.*;

public final class Invers {
	
	public Invers() {
		
	}
	
	public static Fractie invers(Numar n) {
		return NumberFactory.getFractie(n.numitor.value, n.numarator.value);
	}
}
