package Operatori;

import Elements.Element;
import Elements.MathElement;
import Geometrie.Angle;
import Numere.Numar;

public final class BetweenLeftStrict extends MathElement{
	
	public BetweenLeftStrict() {
		
	}
	
	public BetweenLeftStrict(final Element left, final Element mid, final Element right) {
		super(left, mid, right);
	}
	
	public static boolean calculate(final Numar n, final Numar left, final Numar right) {
		return Greater.calculate(n, left) && LessEqual.calculate(n, right);
	}
	
	public static boolean calculate(final Angle u, final Angle left, final Angle right) {
		return Greater.calculate(u, left) && LessEqual.calculate(u, right);
	}
	
	public String toString() {
		if (left != null && right != null && right != null) {
			return left + " < " + mid + "\\le" + right;
		} else {
			return "";
		}
	}
}
