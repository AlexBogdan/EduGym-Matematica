package Operatori;

import Elements.Element;
import Elements.MathElement;
import Geometrie.Angle;
import Numere.Numar;

public final class BetweenRightStrict extends MathElement {
	
	public BetweenRightStrict() {
		
	}
	
	public BetweenRightStrict(final Element left, final Element mid, final Element right) {
		super(left, mid, right);
	}
	
	public static boolean calculate(final Numar n, final Numar left, final Numar right) {
		return GreaterEqual.calculate(n, left) && Less.calculate(n, right);
	}
	
	public static boolean calculate(final Angle u, final Angle left, final Angle right) {
		return GreaterEqual.calculate(u, left) && Less.calculate(u, right);
	}
	
	public String toString() {
		if (left != null && right != null && right != null) {
			return left + " \\le " + mid + "<" + right;
		} else {
			return "";
		}
	}
}
