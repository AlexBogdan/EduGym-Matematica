package Operatori;

import Elements.Element;
import Elements.MathElement;
import Geometrie.Angle;
import Numere.Numar;

public final class Less extends MathElement {
	
	public Less() {
		
	}
	
	public Less(final Element left, final Element right) {
		super(left, right);
	}
	
	public static boolean calculate(final Numar n1, final Numar n2) {
		return n1.value < n2.value && NonEqual.calculate(n1, n2);
	}
	
	public static boolean calculate(final Angle u1, final Angle u2) {
		if (Equal.calculate(u1.grade, u2.grade)) {
			if (Equal.calculate(u1.minute, u2.minute)) {
				return Less.calculate(u1.secunde, u2.secunde);
			} else {
				return Less.calculate(u1.minute, u2.minute);
			}
		} else {
			return Less.calculate(u1.grade, u2.grade);
		}
	}
	
	public String toString() {
		if (left != null && right != null) {
			return left + " < " + right;
		} else {
			return "<";
		}
	}
}
