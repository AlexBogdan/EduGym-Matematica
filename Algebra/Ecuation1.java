package Algebra;

import Elements.MathElement;
import Elements.Variable;
import Numere.Natural;
import Numere.Numar;
import Operatori.Impartire;
import Operatori.Minus;
import Operatori.NonEqual;
import Operatori.Positive;

/**
 * Rezolva o ecuatie de forma de ax + b = c
 * @author arbiter
 *
 */
public class Ecuation1 {
	
	// a este x.coef
	private MathElement b = null;
	private MathElement c = null;
	
	private Variable x;
	
	public Ecuation1(Variable x, MathElement b) {
		this.x = x;
		this.b = b;
	}
	
	public Ecuation1(Variable x, MathElement b, MathElement c) {
		this.x = x;
		this.b = b;
		this.c = c;
	}
	
	public Numar solve() {
		return Impartire.calculate(Minus.calculate((Numar)c, (Numar)b), x.coef);
	}
	
	// Aici mai e de lucrat (fara cast daca nu avem instanceof)
	public String toString() {
		if (NonEqual.calculate((Natural) b,  new Natural(0))) {
			if (Positive.calculate((Natural) b)) {
				return "" + x + "+" + b + "=" + c;
			} else {
				return "" + x + b + "=" + c;
			}
		} else {
			return "" + x + "=" + c;
		}
	}
}
