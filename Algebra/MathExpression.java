package Algebra;

import java.util.ArrayList;

import Elements.MathElement;

public class MathExpression extends MathElement{
	
		public ArrayList<MathElement> list = new ArrayList<MathElement>();
		
		public MathExpression() {
			
		}
		
		public void addElement(final MathElement e) {
			list.add(e);
		}
		
		public String toString() {
			String result = "";
			
			for (MathElement el : list) {
				result += el;
			}
			
			return result;
		}
}
