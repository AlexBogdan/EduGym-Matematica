package Algebra;


import java.util.ArrayList;
import java.util.LinkedHashMap;

import Elements.MathElement;
import Elements.Variable;
import Numere.Natural;
import Numere.Numar;

public class Polinom extends MathElement {
	
	private LinkedHashMap<Natural, Variable> variables = new LinkedHashMap<Natural, Variable>();
	
	public Polinom() {
		
	}
	
	public Polinom(LinkedHashMap<Natural, Variable> variables) {
		this.variables = variables;
	}
	
	public void setVariable(final Variable x) {
		if (x.pow.isNatural()) {
			variables.put(new Natural(x.pow), x);
		}
	}
	
	public String toString() {
		String result = "";
		
		for (Variable x : variables.values()) {
			result += x;
		}
		if (result.charAt(0) == '+') {
			return result.substring(1, result.length());
		}
		return result;
	}
}
